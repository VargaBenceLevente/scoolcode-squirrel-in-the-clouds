This repository is a memory for myself.

# The program

The intention was to create a simple Super Mario, Temple Run, or Doodle Jump clone in 2D, and in the process, push the engine to its limits.

Some features that I built in an engine that doesn't do by itself:

- Platforming, collision detection, and more realistic movement in a 2D platforming world

- Vertical chunks for platforms and pickups so that the single-threaded browser doesn't need to think about out of bounds things

- Camera movement, camera shake, but still mostly centered on the character

- Tinting: day-night cycle, tint value caching

- Menu stack

Note: comments are phrased weirdly simply because in my opinion, the environment necessitated them to be phrased in a more instructional manner.

P.S. The engine at the time was not built to handle this much work, but it was optimized enough for an okay computer to run it well.